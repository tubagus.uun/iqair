import SignInAPI, {
    SignInResetStatusAPI,
    SignOutAPI,
    RestoreTokenAPI,
    AppLoadedAPI
} from '../APIs/SignInAPI'

export const SignInAction = (username, password) => {
    return (dispatch) => {
        return (SignInAPI(dispatch, username, password))
    };
}

export const SignInResetStatusAction = () => {
    return (dispatch) => {
        return (SignInResetStatusAPI(dispatch))
    };
}

export const SignOutAction = () => {
    return (dispatch) => {
        return (SignOutAPI(dispatch))
    };
}

export const RestoreTokenAction = () => {
    return (dispatch) => {
        return (RestoreTokenAPI(dispatch))
    };
}
