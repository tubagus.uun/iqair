import {
    AvGetListCountryAPI,
    AvGetListStateAPI,
    AvGetListCityAPI,
    AvGetDataCityAPI,
    AvGetDataNearestCityAPI
} from '../APIs/AvAPI'


export const avGetListCountryAction = (token) => {
    // buat dispact loading
    return function (dispatch) {
        return (AvGetListCountryAPI(dispatch, token))
    };
}

export const avGetListStateAction = (country, token) => {
    // buat dispact loading, fungsi disederhanakan
    return (dispatch) => {
        return (AvGetListStateAPI(dispatch, country, token))
    };
}

export const avGetListCityAction = (country, state, token) => {
    // buat dispact loading, fungsi disederhanakan
    return (dispatch) => {
        return (AvGetListCityAPI(dispatch, country, state, token))
    };
}

export const avGetDataCityAction = (country, state, city, token) => {
    // buat dispact loading, fungsi disederhanakan
    return (dispatch) => {
        return (AvGetDataCityAPI(dispatch, country, state, city, token))
    };
}

export const avGetDataNearestCityAction = (token) => {
    // buat dispact loading, fungsi disederhanakan
    return (dispatch) => {
        return (AvGetDataNearestCityAPI(dispatch, token))
    };
}