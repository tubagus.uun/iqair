import React from 'react'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import reducers from './Reducers/index'

import  Navigator from './Screens/Navigator'

const store = createStore(reducers, applyMiddleware(thunk))


export default AppContainer = () => {
    return(
        <Provider store={store}>
            <Navigator />
        </Provider>
    );
}
