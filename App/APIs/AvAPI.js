import {
    GET_LIST_COUNTRIES_REQUEST,
    GET_LIST_COUNTRIES_FAILURE,
    GET_LIST_COUNTRIES_SUCCESS,

    GET_LIST_STATES_REQUEST,
    GET_LIST_STATES_FAILURE,
    GET_LIST_STATES_SUCCESS,

    GET_LIST_CITIES_REQUEST,
    GET_LIST_CITIES_FAILURE,
    GET_LIST_CITIES_SUCCESS,

    GET_DATA_CITY_REQUEST,
    GET_DATA_CITY_FAILURE,
    GET_DATA_CITY_SUCCESS,

    GET_DATA_NEAREST_CITY_REQUEST,
    GET_DATA_NEAREST_CITY_FAILURE,
    GET_DATA_NEAREST_CITY_SUCCESS,

    GET_LIST_STATIONS,
    GET_DATA_STATION,
    GET_DATA_NEAREST_STATION,
} from '../Iface/Types';

import Get, { hosturl } from '../Utils/AxioAPI'


export const AvGetListCountryAPI = (dispatch, token) => {

    dispatch({
        type: GET_LIST_COUNTRIES_REQUEST
    });

    callbackOK = (json) => {
        dispatch({
            type: GET_LIST_COUNTRIES_SUCCESS,
            status: 200,
            data: json.data
        });
    }

    callbackErr = (err) => {
        let stat = 200
        let msg = 'ok'

        if (err.response) {
            stat = err.response.status
            msg = err.response.data.data.message
            if(msg== 'call_per_day_limit_reached')
                msg = 'call per day limit reached'
        } else {
            stat = 500
            msg = "error cuy"
        } 
        dispatch({
            type: GET_LIST_COUNTRIES_FAILURE,
            status: stat,
            msg: msg
        });
    }

    let url = `${hosturl}countries?key=${token}`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export const AvGetListStateAPI = (dispatch, country, token) => {

    dispatch({
        type: GET_LIST_STATES_REQUEST
    });

    callbackOK = (json) => {
        dispatch({
            type: GET_LIST_STATES_SUCCESS,
            data: json.data,
            status: 200,
            country: country
        });
    }
    callbackErr = (err) => {
        let stat = 200
        let msg = 'ok'

        if (err.response) {
            stat = err.response.status
            msg = err.response.data.data.message
            if(msg== 'call_per_day_limit_reached')
                msg = 'call per day limit reached'
        } else {
            stat = 500
            msg = "error cuy"
        } 
        dispatch({
            type: GET_LIST_STATES_FAILURE,
            status: stat,
            msg: msg
        });
    }
    let url = `${hosturl}states?country=${country}&key=${token}`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export const AvGetListCityAPI = (dispatch, country, stateprov, token) => {

    dispatch({
        type: GET_LIST_CITIES_REQUEST
    });

    callbackOK = (json) => {
        dispatch({
            type: GET_LIST_CITIES_SUCCESS,
            data: json.data,
            status: 200,
            country: country,
            stateprov: stateprov
        });
    }
    callbackErr = (err) => {
        let stat = 200
        let msg = 'ok'

        if (err.response) {
            stat = err.response.status
            msg = err.response.data.data.message
            if(msg== 'call_per_day_limit_reached')
                msg = 'call per day limit reached'
        } else {
            stat = 500
            msg = "error cuy"
        } 
        dispatch({
            type: GET_LIST_CITIES_FAILURE,
            status: stat,
            msg: msg
        });
    }
    let url = `${hosturl}cities?country=${country}&state=${stateprov}&key=${token}`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export const AvGetDataCityAPI = (dispatch, country, stateprov, city, token) => {

    dispatch({
        type: GET_DATA_CITY_REQUEST
    });

    callbackOK = (json) => {
        dispatch({
            type: GET_DATA_CITY_SUCCESS,
            data: json,
            status: 200,
            country: country,
            stateprov: stateprov,
            city: city
        });
    }
    
    callbackErr = (err) => {
        let stat = 200
        let msg = 'ok'

        if (err.response) {
            stat = err.response.status
            msg = err.response.data.data.message
            if(msg== 'call_per_day_limit_reached')
                msg = 'call per day limit reached'
        } else {
            stat = 500
            msg = "error cuy"
        } 
        dispatch({
            type: GET_DATA_CITY_FAILURE,
            status: stat,
            msg: msg
        });
    }
    let url = `${hosturl}city?country=${country}&state=${stateprov}&city=${city}&key=${token}`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export const AvGetDataNearestCityAPI = (dispatch, token) => {

    dispatch({
        type: GET_DATA_NEAREST_CITY_REQUEST
    });

    callbackOK = (json) => {
        dispatch({
            type: GET_DATA_NEAREST_CITY_SUCCESS,
            status: 200,
            data: json,
        });
    }
    callbackErr = (err) => {
        let stat = 200
        let msg = 'ok'

        if (err.response) {
            stat = err.response.status
            msg = err.response.data.data.message
            if(msg== 'call_per_day_limit_reached')
                msg = 'call per day limit reached'
        } else {
            stat = 500
            msg = "error cuy"
        } 
        dispatch({
            type: GET_DATA_NEAREST_CITY_FAILURE,
            status: stat,
            msg: msg
        });
    }
    let url = `${hosturl}nearest_city?key=${token}`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export default AvGetListCountryAPI