import {
    SIGN_IN_REQUEST,
    SIGN_IN_SUCCESS,
    SIGN_IN_FAILURE,
    SIGN_IN_RESET_STATUS,

    RESTORE_TOKEN,
    APP_LOADED,
    SIGN_OUT,

} from '../Iface/Types';

import Get, { authurl } from '../Utils/AxioAPI'
import GetKey, { SetKey, ClearAll } from '../Utils/AsynStorageAPI'

const SignInAPI = (dispatch, username, password) => {

    dispatch({
        type: SIGN_IN_REQUEST
    });

    callbackStorage = (json) => {
        dispatch({
            type: SIGN_IN_SUCCESS,
            token: json.token,
            username: json.username
        });
    }

    callbackOK = (json) => {
        if (json == null) {
            let err = {
                response: {
                    status: 404,
                    message: "user tidak valid"
                }
            }
            throw err
        }
        let fbusername = json.name
        let fbpassword = json.password
        let fbprofession = json.profession
        let fbtoken = json.token
        if ((username.toLowerCase() === fbusername) && (password === fbpassword)) {
            let userkey = {username:fbusername, token:fbtoken}
            SetKey(userkey, callbackStorage)
        } else {
            dispatch({
                type: SIGN_IN_FAILURE,
                code: 403,
                msg: "Auth Invalid"
            });
        }
    }
    callbackErr = (err) => {
        let code = 200
        let msg = 'ok'
        if (err.response) {
            code = err.response.status
            if (err.response.status == 404)
                msg = "User Tidak Terdaftar"
            else
                msg = "Error " + stat
        } else {
            stat = 500
            msg = "error cuy"
        }
        dispatch({
            type: SIGN_IN_FAILURE,
            code: code,
            msg: msg
        });
    }
    let url = `${authurl}${username.toLowerCase()}.json`
    // console.log(url)
    Get(url, callbackOK, callbackErr)
}

export const SignInResetStatusAPI = (dispatch) => {
    dispatch({
        type: SIGN_IN_RESET_STATUS
    });
}

export const SignOutAPI = (dispatch) => {
    callbackStorage = () => {
        dispatch({
            type: SIGN_OUT
        });
    }
    ClearAll(callbackStorage);
}

export const RestoreTokenAPI = (dispatch) => {
    callbackStorage = (json) => {
        if (json == null) {
            dispatch({
                type: APP_LOADED
            });
        } else {
            dispatch({
                type: RESTORE_TOKEN,
                token: json.token,
                username: json.username
            });
        }
    }
    GetKey(callbackStorage);
}

export default SignInAPI