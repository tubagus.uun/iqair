export const GET_LIST_COUNTRIES_REQUEST = 'GET_LIST_COUNTRIES_REQUEST'
export const GET_LIST_COUNTRIES_FAILURE = 'GET_LIST_COUNTRIES_FAILURE'
export const GET_LIST_COUNTRIES_SUCCESS = 'GET_LIST_COUNTRIES_SUCCESS'

export const GET_LIST_STATES_REQUEST = 'GET_LIST_STATES_REQUEST'
export const GET_LIST_STATES_FAILURE = 'GET_LIST_STATES_FAILURE'
export const GET_LIST_STATES_SUCCESS = 'GET_LIST_STATES_SUCCESS'

export const GET_LIST_CITIES_REQUEST = 'GET_LIST_CITIES_REQUEST'
export const GET_LIST_CITIES_FAILURE = 'GET_LIST_CITIES_FAILURE'
export const GET_LIST_CITIES_SUCCESS = 'GET_LIST_CITIES_SUCCESS'

export const GET_DATA_CITY_REQUEST = 'GET_DATA_CITY_REQUEST'
export const GET_DATA_CITY_FAILURE = 'GET_DATA_CITY_FAILURE'
export const GET_DATA_CITY_SUCCESS = 'GET_DATA_CITY_SUCCESS'

export const GET_DATA_NEAREST_CITY_REQUEST = 'GET_DATA_NEAREST_CITY_REQUEST'
export const GET_DATA_NEAREST_CITY_FAILURE = 'GET_DATA_NEAREST_CITY_FAILURE'
export const GET_DATA_NEAREST_CITY_SUCCESS = 'GET_DATA_NEAREST_CITY_SUCCESS'


export const GET_LIST_STATIONS = 'GET_LIST_STATIONS'
export const GET_DATA_STATION = 'GET_DATA_STATION'
export const GET_DATA_NEAREST_STATION = 'GET_DATA_NEAREST_STATION'

export const SIGN_IN_REQUEST = 'SIGN_IN_REQUEST'
export const SIGN_IN_SUCCESS = 'SIGN_IN_SUCCESS'
export const SIGN_IN_FAILURE = 'SIGN_IN_FAILURE'
export const SIGN_IN_RESET_STATUS = 'SIGN_IN_RESET_STATUS'
export const RESTORE_TOKEN = 'RESTORE_TOKEN'
export const APP_LOADED = 'APP_LOADED'
export const SIGN_OUT = 'SIGN_OUT'




/*
GET List supported countries
api.airvisual.com/v2/countries?key={{YOUR_API_KEY}}

GET List supported states in a country
api.airvisual.com/v2/states?country={{COUNTRY_NAME}}&key={{YOUR_API_KEY}}

GET List supported cities in a state
api.airvisual.com/v2/cities?state={{STATE_NAME}}&country={{COUNTRY_NAME}}&key={{YOUR_API_KEY}}

GET Get specified city data
api.airvisual.com/v2/city?city=Los Angeles&state=California&country=USA&key={{YOUR_API_KEY}}

GET Get nearest city data (IP geolocation)
api.airvisual.com/v2/nearest_city?key={{YOUR_API_KEY}}


GET List supported stations in a city
api.airvisual.com/v2/stations?city={{CITY_NAME}}&state={{STATE_NAME}}&country={{COUNTRY_NAME}}&key={{YOUR_API_KEY}}

GET Get nearest station data (IP geolocation)
api.airvisual.com/v2/nearest_station?key={{YOUR_API_KEY}}

GET Get specified station data
api.airvisual.com/v2/station?station={{STATION_NAME}}&city={{CITY_NAME}}&state={{STATE_NAME}}&country={{COUNTRY_NAME}}&key={{YOUR_API_KEY}}

*/