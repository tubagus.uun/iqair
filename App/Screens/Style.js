import { StyleSheet } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFFF'
    },
    btlist: {
        alignItems: "center",
        backgroundColor: "#3EC6FF",
        padding: 10,
        borderRadius: 16,
        marginHorizontal: 30,
        marginBottom: 10,
        width: 140
    },
    btText: {
        fontFamily: 'Montserrat_regular'
    },
    btnsText: {
        fontFamily: 'Montserrat_bold'
    },
    title: {
        marginLeft: 10,
        fontFamily: 'Montserrat_bold'
    },
    titleContainer: {
        padding: 8,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white',
        borderBottomLeftRadius: 10,
        borderBottomRightRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    refreshButton: {
        position:'absolute'
        ,zIndex:11,
        right:20,
        bottom:20,
        backgroundColor:'#FFFF',
        width:70,
        height:70,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center',
        shadowColor: "#000", shadowOffset: { width: 0, height: 4, }, shadowOpacity: 0.30, shadowRadius: 4.65, elevation: 8,
    }
});

export const HomeStyles = StyleSheet.create({
    item: {
        flex: 1,
        backgroundColor: '#FFFF'
    },  
    bgImage: {
        width: wp(100),
        height: hp(40),
    },
    bgImageContainer: {
        flex: 1,
    },
    TextContainer: {
        flex: 10,
        textAlign: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    greetings: {
        fontSize: hp(4),
        color: '#FFFF',
        fontFamily: 'Montserrat_bold',
        marginBottom: 15
    },
    city: {
        fontSize: hp(3),
        color: '#FFFF',
        fontFamily: 'Montserrat_medium',
    },
    country: {
        fontSize: hp(3),
        color: '#FFFF',
        fontFamily: 'Montserrat_medium',
        marginBottom: 5
    },
    lastUp: {
        fontSize: hp(1.1),
        color: '#000',
        fontFamily: 'Montserrat_medium',
    },
    imageContainer: {
        alignItems: 'center',
        // backgroundColor: '#87cefa',
        flex: 25
    },
    image: {
        width: hp(25),
        height: hp(20),
        marginTop: 120
    },
    tempContainer: {
        flex: 30,
        // backgroundColor: '#87cefa',
        alignItems: 'center'
    },
    temp: {
        fontSize: hp(7),
        fontFamily: 'Montserrat_medium',
        marginTop: 105
    },
    boxContainer1: {
        flex: 20,
        // backgroundColor: '#87cefa',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    boxContainer2: {
        flex: 20,
        // backgroundColor: '#87cefa',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    box1: {
        marginTop: 55,
        height: hp(10),
        padding: 8,
        margin: 10,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    box2: {
        marginTop: 10,
        height: hp(10),
        padding: 10,
        marginHorizontal: 17,
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    titleWet: {
        fontFamily: 'Montserrat_bold',
        fontSize: hp(1.7),
        marginTop: 8
    },
    ket: {
        fontFamily: 'Montserrat_medium',
        fontSize: hp(1.4),
        marginTop: 7
    }
})

export const AboutStyles = StyleSheet.create({
    container: {
        flex: 1,
    },  
    profileContainer: {
        flex: 0.7,
        backgroundColor: '#28437a',
    },
    image: {
        width: wp(28),
        height: hp(15),
        borderRadius: 100,
    },
    imageContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 24
    },
    namaContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10
    },
    nama: {
        fontFamily: 'Montserrat_bold',
        color: '#fff',
        fontSize: hp(2)
    },
    akunContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 10
    },
    akun: {
        fontFamily: 'Montserrat_regular',
        color: '#fff',
        fontSize: hp(1.4),
    },
    job: {
        fontFamily: 'Montserrat_medium',
        color: '#fff',
        fontSize: hp(1.7),
    },
    sourceText: {
        textAlign: 'center',
        fontFamily: 'Montserrat_bold',
        fontSize: hp(4),
        marginTop: 24,   
    },
    urlContainer: {
        justifyContent: 'center',
        marginTop: 15,
        alignItems: 'center'
    },
    url: {
        fontFamily: 'Montserrat_regular',
        fontSize: hp(1.75)
    },
    ketContainer: {
        flex: 1,
        backgroundColor: '#ffff'
    },
    loginButton: {
        backgroundColor: '#28437a',
        width: wp(30),
        height: hp(7),
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginTop: 50
    },
    buttonText: {
        fontSize: hp(2.5),
        color: 'white',
        fontFamily: 'Montserrat_bold'
    },
})