import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';
import { connect } from 'react-redux'
import CountryListScreen from './CountryListScreen';
import StateListScreen from './StateListScreen';
import CityListScreen from './CityListScreen';
import CityDataScreen from './CityDataScreen'
import HomeDataScreen from './HomeDataScreen'
import LoginScreen from './LoginScreen';
import AboutScreen from './AboutScreen';
import SplashScreen from './SplashScreen';

import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

const RootStack = createStackNavigator();
const CuacaStack = createStackNavigator();
const TabsStack = createBottomTabNavigator();
const DrawerStack = createDrawerNavigator();


function getHeaderTitle(route) {
    const routeName = getFocusedRouteNameFromRoute(route) ?? 'Home';
    switch (routeName) {
        case 'CountiesScreen':
            return 'Countries';
        case 'StatesScreen':
            return 'States';
        case 'CitiesScreen':
            return 'Cities';
        case 'Home':
            return 'Home';
        case 'Country':
            return 'Countries';
        case 'About':
            return 'About Us';
    }
}

const iconTab = ({ route }) => {
    return ({
        tabBarIcon: ({ focused, color, size }) => {
            if (route.name == 'Home') {
                return <FontAwesome5 name={'centos'} size={size} color={color} solid />
            } else if (route.name == 'Country') {
                return <FontAwesome5 name="globe-asia" size={size} color={color} solid />
            } else if (route.name == 'About') {
                return <FontAwesome5 name="users" size={size} color={color} solid />
            }
        },
    });
}

const CuacaStackScreen = ({ navigation, route }) => {

    return (
        <CuacaStack.Navigator>
            <CuacaStack.Screen name='CountiesScreen' component={CountryListScreen}
                options={{
                    title: 'Countries',
                }} />

        </CuacaStack.Navigator>
    );
}

const TabsStackScreen = ({ navigation, route }) => {
    React.useLayoutEffect(() => {
        navigation.setOptions({ headerTitle: getHeaderTitle(route) });
    }, [navigation, route]);
    return (
        <TabsStack.Navigator screenOptions={iconTab} >
            <TabsStack.Screen name='Home' component={HomeDataScreen}
                options={{
                    title: 'Home',
                }} />
            <TabsStack.Screen name='Country' component={CuacaStackScreen}
                options={{
                    title: 'Countries',
                }} />

            <TabsStack.Screen name='About' component={AboutScreen}
                options={{
                    title: 'About Us',
                }}
            />
        </TabsStack.Navigator>
    );
}


const RootStackScreen = (props) => {
    if (props.isLoading) {
        return (
            <SplashScreen />
        );
    }
    return (
        <RootStack.Navigator>
            {
                props.userToken == null ? (

                    <RootStack.Screen name='LoginScreen' component={LoginScreen}
                        options={{
                            title: 'Login',
                            headerShown: false
                        }}
                    />
                ) : (
                        <>
                            <RootStack.Screen name='Home' component={TabsStackScreen}
                                options={{
                                    title: 'Home',
                                    headerStyle: {
                                        backgroundColor: '#28437a',
                                    },
                                    headerTintColor: '#fff'
                                }}
                            />
                            <RootStack.Screen name='StatesScreen' component={StateListScreen}
                                options={{
                                    title: 'States',
                                    headerTitle: 'States',
                                    headerStyle: {
                                        backgroundColor: '#28437a',
                                    },
                                    headerTintColor: '#fff',
                                }} />
                            <RootStack.Screen name='CitiesScreen' component={CityListScreen}
                                options={{
                                    title: 'Cities',
                                    headerStyle: {
                                        backgroundColor: '#28437a',
                                    },
                                    headerTintColor: '#fff',
                                }} />
                            <RootStack.Screen name='DataCityScreen' component={CityDataScreen}
                                options={{
                                    title: 'City',
                                    headerStyle: {
                                        backgroundColor: '#28437a',
                                    },
                                    headerTintColor: '#fff',
                                }} />
                        </>
                    )}


        </RootStack.Navigator>
    );
}

const mapStateToProp = (state) => {
    return {
        userToken: state.si.userToken,
        isSignOut: state.si.isSignOut,
        isLoading: state.si.isLoading,
    }
}
let RootReduxStackScreen = connect(mapStateToProp)(RootStackScreen);

export default () => (
    <SafeAreaProvider>
        <NavigationContainer>
            {/* <TabsStackScreen /> */}
            {/* <RootStackScreen /> */}
            <RootReduxStackScreen />
            {/* <SplashScreen /> */}
        </NavigationContainer>
    </SafeAreaProvider>

);




