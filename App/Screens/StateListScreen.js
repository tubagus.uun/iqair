import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, TouchableOpacity, ActivityIndicator, Alert, TextInput } from 'react-native';
import { connect } from 'react-redux'
import { avGetListStateAction } from '../Actions'
import { styles } from './Style'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

function Item({ item, navigation, country }) {
    return (
        <View style={styles.item}>
            <TouchableOpacity style={styles.titleContainer}
                onPress={() => navigation.navigate('CitiesScreen', {
                    country: country,
                    state: item.state
                })} >
                <Text style={styles.btText}>  {item.state} </Text>
            </TouchableOpacity>
        </View>
    );
}

function App(props) {

    const [isCalled, setIsCalled] = useState(true);
    const [keySearch, setKeySearch] = useState('');

    useEffect(() => {
        // dipanggil pada saat perubahan state
        props.avGetListStateAction(props.route.params.country, props.token)
        return () => { }
    }, [isCalled]);

    useEffect(() => {
        if (props.reqstat != 200) {
            Alert.alert(`Error ${props.reqstat}`, props.reqmsg)
        }
        return () => { }
    }, []);

    const onPressbt = () => {
        props.avGetListStateAction(props.route.params.country, props.token)
    }

    const renderItem = ({ item }) => {
        if (item.state.toLowerCase().includes(keySearch.toLowerCase())) {
            return (
                <Item item={item} navigation={props.navigation}
                    country={props.country} />
            );
        }
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.refreshButton} onPress={onPressbt}>
                <FontAwesome5 name={'sync'} size={25} />
            </TouchableOpacity>
            <TextInput style={{
                padding: 8,
                fontFamily: 'Montserrat_regular',
                marginVertical: 5,
                borderWidth: 1,
                borderColor: 'gray',
                borderRadius: 24,
            }} placeholder='search here...'
                onChangeText={text => setKeySearch(text)} />
                
            {
                props.isLoading ?
                    (
                        <ActivityIndicator />
                    ) : (
                        <FlatList
                            data={props.items} renderItem={renderItem}
                            keyExtractor={item => item.state}
                        />
                    )
            }
            <StatusBar style="auto" />
        </View>
    );
}

const mapStateToProp = (state) => {
    return {
        items: state.av.stateList,
        isLoading: state.av.stateListReqProc,
        reqmsg: state.av.stateListReqErrMsg,
        reqstat: state.av.stateListReqStatus,
        country: state.av.country,
        token: state.si.userToken
    }
}

export default connect(mapStateToProp, { avGetListStateAction })(App)

