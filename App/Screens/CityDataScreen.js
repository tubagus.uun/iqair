import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { Image, Text, View, ActivityIndicator, Alert } from 'react-native';
import { connect } from 'react-redux'
import { avGetDataCityAction } from '../Actions'
import { styles, HomeStyles} from './Style'

function Item({ item, username }) {
    return (
        <View style={HomeStyles.item}>
            <View style={HomeStyles.bgImageContainer}>
                <Image source={require('../../assets/23284.jpg')} style={HomeStyles.bgImage} resizeMode='cover'/>
            </View>

            <View style={HomeStyles.TextContainer}> 
            <Text style={HomeStyles.greetings}>Hi, {username}</Text>
                <Text style={HomeStyles.city}>{item.city}, {item.state}</Text>
                <Text style={HomeStyles.country}>{item.country}</Text>
                <Text style={HomeStyles.lastUp}>Last update : {Date(item.current.weather.ts)}</Text>
            </View>

            <View style={HomeStyles.imageContainer}>
            <Image
                style={HomeStyles.image}
                source={{ uri: 'https://www.airvisual.com/images/' + item.current.weather.ic + '.png' }}
            />
            </View>
            
            <View style={HomeStyles.tempContainer}>
             <Text style={HomeStyles.temp}>{item.current.weather.tp}°C</Text>
            </View>

            <View style={HomeStyles.boxContainer1}>
                    <View style={HomeStyles.box1}>
                        <Text style={HomeStyles.titleWet}>Pressure</Text>
                        <Text style={HomeStyles.ket}>{item.current.weather.pr} hPa</Text>
                    </View>
                    <View style={HomeStyles.box1}>
                        <Text style={HomeStyles.titleWet}>Humidity</Text>
                        <Text style={HomeStyles.ket}>{item.current.weather.hu} %</Text>
                    </View>
                    <View style={HomeStyles.box1}>
                        <Text style={HomeStyles.titleWet}>Wind Speed</Text>
                        <Text style={HomeStyles.ket}>{item.current.weather.ws} m/s</Text>
                    </View>
                </View>

                <View style={HomeStyles.boxContainer2}>
                <View style={HomeStyles.box2}>
                        <Text style={HomeStyles.titleWet}>AQIus</Text>
                        <Text style={HomeStyles.ket}>{item.current.pollution.aqius}</Text>
                    </View>
                <View style={HomeStyles.box2}>
                    <Text style={HomeStyles.titleWet}>AQIcn</Text>
                    <Text style={HomeStyles.ket}>{item.current.pollution.aqicn}</Text>
                </View>
                <View style={HomeStyles.box2}>
                    <Text style={HomeStyles.titleWet}>Pollutant</Text>
                    <Text style={HomeStyles.ket}>{item.current.pollution.mainus}</Text>
                </View>
                </View>
            </View>
    );
}

function App(props) {

    const [isCalled, setIsCalled] = useState(true);

    useEffect(() => {
        // dipanggil pada saat perubahan state
        props.avGetDataCityAction(props.route.params.country,
            props.route.params.state,
            props.route.params.city, props.token)
        return () => { }
    }, [isCalled]);

    useEffect(() => {
        if (props.reqstat != 200) {
            Alert.alert(`Error ${props.reqstat}`, props.reqmsg)
        }
        return () => { }
    }, []);

    const onPressbt = () => {
        props.avGetDataCityAction(props.route.params.country,
            props.route.params.state,
            props.route.params.city, props.token)
    }

    if(props.isLoading) {
        return (<View style={styles.container}><ActivityIndicator /></View>)
    }else{
        return (
            <View style={styles.container}>
                {/* <Text>City Data Screen</Text>
                <Button onPress={onPressbt} title="Pencet tombol" /> */}
                {
                    props.detail.data ?
                        (
                            <Item item={props.detail.data} username={props.username} />
                        ) : (
                            <></>
                        )
                }
                <StatusBar style="auto" />
            </View>
        );
    }
    
}

const mapStateToProp = (state) => {
    return {
        detail: state.av.dataCity,
        isLoading: state.av.dataCityReqProc,
        reqmsg: state.av.dataCityReqErrMsg,
        reqstat: state.av.dataCityReqStatus,

        country: state.av.country,
        state: state.av.stateprov,
        city: state.av.city,
        token: state.si.userToken,
        username: state.si.username
    }
}

export default connect(mapStateToProp, { avGetDataCityAction })(App)

