import { StatusBar } from 'expo-status-bar';
import React, { useEffect, useState } from 'react';
import { Text, View, FlatList, TouchableOpacity, ActivityIndicator, Alert, TextInput } from 'react-native';
import { connect } from 'react-redux'
import { avGetListCityAction } from '../Actions'
import { styles } from './Style'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

function Item({ item, navigation, country, state }) {
    return (
        <View style={styles.item}>
            <TouchableOpacity style={styles.titleContainer}
                onPress={() => navigation.navigate('DataCityScreen', {
                    country: country,
                    state: state,
                    city: item.city
                })} >
                <Text style={styles.btText}>  {item.city} </Text>
            </TouchableOpacity>
        </View>
    );
}

function App(props) {

    const [isCalled, setIsCalled] = useState(true);
    const [keySearch, setKeySearch] = useState('');

    useEffect(() => {
        // dipanggil pada saat perubahan state
        props.avGetListCityAction(props.route.params.country,
            props.route.params.state, props.token)
        return () => { }
    }, [isCalled]);

    useEffect(() => {
        if (props.reqstat != 200) {
            Alert.alert(`Error ${props.reqstat}`, props.reqmsg)
        }
        return () => { }
    }, []);

    const onPressbt = () => {
        props.avGetListCityAction(props.route.params.country,
            props.route.params.state, props.token)
    }

    const renderItem = ({ item }) => {
        if (item.city.toLowerCase().includes(keySearch.toLowerCase())) {
            return (
                <Item item={item} navigation={props.navigation} country={props.country}
                    state={props.state} />
            );
        }
    };

    return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.refreshButton} onPress={onPressbt}>
                <FontAwesome5 name={'sync'} size={25} />
            </TouchableOpacity>
            <TextInput style={{
                padding: 8,
                fontFamily: 'Montserrat_regular',
                marginVertical: 5,
                borderWidth: 1,
                borderColor: 'gray',
                borderRadius: 24,
            }} placeholder='search here...'
                onChangeText={text => setKeySearch(text)} />

            {
                props.isLoading ?
                    (
                        <ActivityIndicator />
                    ) : (
                        <FlatList
                            data={props.items}
                            renderItem={renderItem}
                            keyExtractor={item => item.city}
                        />
                    )
            }
            <StatusBar style="auto" />
        </View>
    );
}

const mapStateToProp = (state) => {
    return {
        items: state.av.cityList,
        isLoading: state.av.cityListReqProc,
        reqmsg: state.av.cityListReqErrMsg,
        reqstat: state.av.stateListReqStatus,

        country: state.av.country,
        state: state.av.stateprov,
        token: state.si.userToken
    }
}

export default connect(mapStateToProp, { avGetListCityAction })(App)

