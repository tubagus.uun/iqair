import React from 'react';
import { Image, TouchableOpacity, Text, View } from 'react-native';
import { connect } from 'react-redux'
import { AboutStyles } from './Style';
import { SignOutAction } from '../Actions'

class AboutScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    logoutHandler() {
        this.props.SignOutAction()
    }

    render() {
        return (
            <View style={AboutStyles.container}>
                <View style={AboutStyles.profileContainer}>
                    <View style={AboutStyles.imageContainer}>
                        <Image style={AboutStyles.image} source={require('../../assets/photo_2020-07-08_22-10-09.jpg')} />
                        <Image style={AboutStyles.image} source={require('../../assets/photo_2020-07-08_22-09-28.jpg')} />
                    </View>

                    <View style={AboutStyles.namaContainer}>
                        <Text style={AboutStyles.nama}>Tubagus Uun Munawir</Text>
                        <Text style={AboutStyles.nama}>Tubagus Gusti Fauzy</Text>
                    </View>

                    <View style={AboutStyles.akunContainer}>
                        <Text style={AboutStyles.akun}>@Tubagusuun</Text>
                        <Text style={AboutStyles.akun}>@tubagusgf</Text>
                    </View>

                    <View style={AboutStyles.akunContainer}>
                        <Text style={AboutStyles.job}>Product Programmer</Text>
                        <Text style={AboutStyles.job}>Product Designer</Text>
                    </View>
                </View>

                <View style={AboutStyles.ketContainer}>
                    <Text style={AboutStyles.sourceText}>Air Visual API</Text>
                    <Text style={AboutStyles.sourceText}>+</Text>
                    <Text style={AboutStyles.sourceText}>Freepik Vector</Text>


                    <View style={AboutStyles.urlContainer}>
                        <Text style={AboutStyles.url}>https://bit.ly/3ebiMPd</Text>
                        <TouchableOpacity onPress={() => this.logoutHandler()}>
                            <View style={AboutStyles.loginButton}>
                                <Text style={AboutStyles.buttonText}>Logout</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </View>
        );
    }
}

const mapStateToProp = (state) => {
    return {
        isSignOut: state.si.isSignOut,
    }
}
export default connect(mapStateToProp, {
    SignOutAction,
})(AboutScreen)