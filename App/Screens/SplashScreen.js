import React, { useEffect, useState } from 'react';
import { Image, Text, View } from 'react-native';
import * as SplashScreen from 'expo-splash-screen';
import { Asset } from 'expo-asset';
import { connect } from 'react-redux'
import * as Font from 'expo-font';

import {
    RestoreTokenAction
} from '../Actions'

function App(props) {
    
    useEffect(() => {
        SplashScreen.preventAutoHideAsync();
        return () => { }
    }, []);

    const _cacheResourcesAsync = async () => {
        SplashScreen.hideAsync();

        const modules = [
            require('../../assets/23284.jpg'),
            require('../../assets/photo_2020-07-08_22-10-09.jpg'),
            require('../../assets/photo_2020-07-08_22-09-28.jpg'),
            require('../../assets/fonts/Montserrat-Bold.ttf'),
            require('../../assets/fonts/Montserrat-Medium.ttf'),
            require('../../assets/fonts/Montserrat-Regular.ttf'),
        ];
// cache semua module
        const cacheImages = modules.map(module => {
            return Asset.fromModule(module).downloadAsync();
        });
        await Promise.all(cacheImages);
        await Font.loadAsync({
            Montserrat_bold: require('../../assets/fonts/Montserrat-Bold.ttf'),
            Montserrat_medium: require('../../assets/fonts/Montserrat-Medium.ttf'),
            Montserrat_regular: require('../../assets/fonts/Montserrat-Regular.ttf'),
        });

        // let promisejajal = new Promise((resolve, reject) => {
        //     setTimeout(() => resolve("done!"), 5000)
        // });
        // let resultjajal = await promisejajal; // wait until the promise resolves (*)

        props.RestoreTokenAction();
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#fff',
            alignItems: 'center',
            justifyContent: 'center',
        }}>
            <Image
                source={require('../../assets/cloudSplashGIF.gif')}
                onLoad={_cacheResourcesAsync}
            />
        </View>
    );

}

const mapStateToProp = (state) => {
    return {
        isLoading: state.si.isLoading,
    }
}
export default connect(mapStateToProp, {
    RestoreTokenAction
})(App);