import { StatusBar } from 'expo-status-bar';
import React from 'react';
import {
    TouchableOpacity,
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    ActivityIndicator,
    Alert,
    Image,
    KeyboardAvoidingView
} from 'react-native';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

import { connect } from 'react-redux'
import {
    SignInAction,
    SignInResetStatusAction,
    SignOutAction,
    RestoreTokenAction
} from '../Actions'

class LoginScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            password: true,
            userName: '',
            passWord: ''
        }
    }

    loginHandler() {
        if (this.state.userName == '' || this.state.passWord == '') {
            Alert.alert('Error', 'Username dan Password Harus diisi');
        } else {
            this.props.SignInAction(this.state.userName, this.state.passWord)
        }
    }

    componentDidUpdate() {
        if (this.props.resCode != 200) {
            Alert.alert('Error ' + this.props.resCode, this.props.resMsg);
            this.props.SignInResetStatusAction()
        }
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS == "ios" ? "padding" : "height"}
                style={styles.container} >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        <View style={styles.bgImageContainer}>
                            <Image source={require('../../assets/23284.jpg')} style={styles.bgImage} resizeMode='cover' />
                        </View>

                        <View style={styles.TextContainer}>
                            <Text style={styles.brand}>IQAir</Text>
                            <Text style={styles.brandText}>The most trusted historical, real-time and forecast air quality data</Text>
                        </View>

                        <View style={styles.loginBox}>
                            <View style={styles.loginTextContainer}>
                                <Text style={styles.loginText}>Login</Text>
                            </View>

                            <View style={styles.formContainer}>
                                <TextInput placeholder='Username' style={styles.inputText} onChangeText={userName => this.setState({ userName })} />
                                <TextInput placeholder='Password' style={styles.inputText} onChangeText={passWord => this.setState({ passWord })} secureTextEntry={this.state.password} />
                                {this.props.isReqLoading ?
                                    (
                                        <ActivityIndicator />
                                    ) : (
                                        <TouchableOpacity onPress={() => this.loginHandler()}>
                                            <View style={styles.loginButton}>
                                                <Text style={styles.buttonText}>Login</Text>
                                            </View>
                                        </TouchableOpacity>
                                    )}


                            </View>

                        </View>

                        <StatusBar style="auto" />
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    bgImage: {
        width: wp(100),
        height: hp(40),
    },
    bgImageContainer: {
        flex: 1
    },
    loginBox: {
        flex: 17,
        backgroundColor: 'white',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 12,
        },
        shadowOpacity: 0.58,
        shadowRadius: 16.00,

        elevation: 24,
    },
    loginTextContainer: {
        flex: 1,
        // backgroundColor: '#87cefa',
        alignItems: 'center'
    },
    loginText: {
        fontSize: hp(5),
        padding: 15,
        fontFamily: 'Montserrat_bold',
        textAlign: 'center',
        marginTop: 30
    },
    formContainer: {
        flex: 2.5,
        // backgroundColor: '#87cefa',
        alignItems: 'center',
    },
    label: {
        fontSize: hp(1.8),
        paddingHorizontal: 15,
        fontFamily: 'Montserrat_medium'
    },
    inputText: {
        width: wp(70),
        height: hp(7),
        backgroundColor: '#FFF',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.29,
        shadowRadius: 4.65,
        elevation: 7,
        borderRadius: 11,
        marginTop: 10,
        padding: 10,
        fontFamily: 'Montserrat_regular',
    },
    iconUser: {
        position: 'absolute',
        top: 90,
        left: 310
    },
    iconPass: {
        position: 'absolute',
        top: 165,
        left: 310
    },
    loginButton: {
        backgroundColor: '#28437a',
        width: wp(30),
        height: hp(7),
        borderRadius: 24,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        marginTop: 30
    },
    buttonText: {
        fontSize: hp(2.5),
        color: 'white',
        fontFamily: 'Montserrat_bold'
    },
    brand: {
        fontSize: hp(7),
        fontFamily: 'Montserrat_bold',
        textAlign: 'center',
        color: '#fff',
        marginTop: 40
    },
    brandText: {
        fontSize: hp(2),
        fontFamily: 'Montserrat_regular',
        textAlign: 'center',
        color: '#fff',
        marginTop: 15
    },
    TextContainer: {
        flex: 10,
        alignItems: 'center',
        backgroundColor: 'transparent',
    },
});

const mapStateToProp = (state) => {
    return {
        isLoading: state.si.isLoading,
        isSignOut: state.si.isSignOut,
        isReqLoading: state.si.signInReqIsLoading,
        resCode: state.si.signInResCode,
        resMsg: state.si.signInResMsg,
        token: state.si.userToken,
        
    }
}

export default connect(mapStateToProp, {
    SignInAction,
    SignInResetStatusAction,
    SignOutAction,
    RestoreTokenAction
})(LoginScreen)
