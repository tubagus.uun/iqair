import AsyncStorage, { useAsyncStorage } from '@react-native-community/async-storage';

const { getItem, setItem, removeItem } = useAsyncStorage('@storage_Key');

export const SetKey = async (key, callback) => {
    let jsonValue = JSON.stringify(key)
    await setItem(jsonValue);
    typeof callback === 'function' && callback(key)
}

export const RemoveKey = async (callback) => {
    let key = await removeItem();
    typeof callback === 'function' && callback()
}

export const ClearAll = async (callback) => {
    await AsyncStorage.clear()
    typeof callback === 'function' && callback()
}

export default GetKey = async (callback) => {
    let key = await getItem();
    // console.log("key : ", key)
    let jsonValue = (key != null) ? JSON.parse(key) : null;
    typeof callback === 'function' && callback(jsonValue)
    // typeof callback === 'function' && callback(key)
}