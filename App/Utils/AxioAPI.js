import axios from 'axios';

export default AxioGet = async (url, callbackOK, callbackErr) => {
    let opt = {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        method: 'GET',
        mode: 'cors',
        cache: 'default',
    };
    try {
        const response = await axios(url, opt);
        const json = await response.data;
        typeof callbackOK === 'function' && callbackOK(json)
    } catch (err) {
        typeof callbackErr === 'function' && callbackErr(err)
    }
}

export const hosturl = 'https://api.airvisual.com/v2/'

// export const hosturl = 'http://192.168.3.246:8081/v2/'
// export const hosturl = 'http://192.168.3.100:8081/v2/'
// export const hosturl = 'http://192.168.0.100:8081/v2/'
export const authurl = "https://tubagus-696bb.firebaseio.com/dbtest/users/"



