import {
    GET_LIST_COUNTRIES_REQUEST,
    GET_LIST_COUNTRIES_FAILURE,
    GET_LIST_COUNTRIES_SUCCESS,

    GET_LIST_STATES_REQUEST,
    GET_LIST_STATES_FAILURE,
    GET_LIST_STATES_SUCCESS,

    GET_LIST_CITIES_REQUEST,
    GET_LIST_CITIES_FAILURE,
    GET_LIST_CITIES_SUCCESS,

    GET_DATA_CITY_REQUEST,
    GET_DATA_CITY_FAILURE,
    GET_DATA_CITY_SUCCESS,

    GET_DATA_NEAREST_CITY_REQUEST,
    GET_DATA_NEAREST_CITY_FAILURE,
    GET_DATA_NEAREST_CITY_SUCCESS,

    GET_LIST_STATIONS,
    GET_DATA_STATION,
    GET_DATA_NEAREST_STATION,
} from '../Iface/Types';


const initiAvState = {

    isCalled: false,

    items: [],
    countryReqStatus: 200,
    countryReqErrMsg: '',
    countryReqProc: false,

    country: '',
    stateList: [],
    stateListReqStatus: 200,
    stateListReqErrMsg: '',
    stateListReqProc: false,

    stateprov: '',
    cityList: [],
    cityListReqStatus: 200,
    cityListReqErrMsg: '',
    cityListReqProc: false,

    city: '',
    dataCity: '',
    dataCityReqStatus: 200,
    dataCityReqErrMsg: '',
    dataCityReqProc: false,

    dataNearestCity: '',
    dataNearestCityReqStatus: 200,
    dataNearestCityReqErrMsg: '',
    dataNearestCityReqProc: false,

    stationList: [],
    station: '',
    key: '54f8ceb9-fd6d-43db-8f6a-5eab7da2bd5f'
};

const reducer = (state = initiAvState, action) => {
    switch (action.type) {

        case GET_LIST_COUNTRIES_REQUEST:
            return {
                ...state,
                countryReqProc: true,
            };
        case GET_LIST_COUNTRIES_SUCCESS:
            return {
                ...state,
                countryReqProc: false,
                items: action.data,
                countryReqStatus: action.status,
            };
        case GET_LIST_COUNTRIES_FAILURE:
            return {
                ...state,
                countryReqProc: false,
                countryReqStatus: action.status,
                countryReqErrMsg: action.msg,
                items: [],
            };


        case GET_LIST_STATES_REQUEST:
            return {
                ...state,
                stateListReqProc: true,
            };
        case GET_LIST_STATES_FAILURE:
            return {
                ...state,
                stateListReqProc: false,
                stateList: [],
                stateListReqStatus: action.status,
                stateListReqErrMsg: action.msg,
            };
        case GET_LIST_STATES_SUCCESS:
            return {
                ...state,
                stateListReqProc: false,
                stateList: action.data,
                stateListReqStatus: action.status,
                country: action.country,
            };

        case GET_LIST_CITIES_REQUEST:
            return {
                ...state,
                cityListReqProc: true,
            };
        case GET_LIST_CITIES_FAILURE:
            return {
                ...state,
                cityListReqProc: false,
                cityList: [],
                cityListReqStatus: action.status,
                cityListReqErrMsg: action.msg,
            };
        case GET_LIST_CITIES_SUCCESS:
            return {
                ...state,
                cityListReqProc: false,
                cityList: action.data,
                cityListReqStatus: action.status,
                country: action.country,
                stateprov: action.stateprov,
            };


        case GET_DATA_CITY_REQUEST:
            return {
                ...state,
                dataCityReqProc: true,
            };
        case GET_DATA_CITY_SUCCESS:
            return {
                ...state,
                dataCityReqProc: false,
                dataCity: action.data,
                dataCityReqStatus: action.status,

                country: action.country,
                stateprov: action.stateprov,
                city: action.city
            };
        case GET_DATA_CITY_FAILURE:
            return {
                ...state,
                dataCityReqProc: false,
                dataCity: '',
                dataCityReqStatus: action.status,
                dataCityReqErrMsg:  action.msg,
            };


        case GET_DATA_NEAREST_CITY_REQUEST:
            return {
                ...state,
                dataNearestCityReqProc: true,
            };
        case GET_DATA_NEAREST_CITY_FAILURE:
            return {
                ...state,
                dataNearestCityReqProc: false,
                dataNearestCityReqStatus: action.status,
                dataNearestCityReqErrMsg: action.msg,
                dataNearestCity: '',
            };
        case GET_DATA_NEAREST_CITY_SUCCESS:
            return {
                ...state,
                dataNearestCityReqProc: false,
                dataNearestCityReqStatus: action.status,
                dataNearestCity: action.data,
            };


        case GET_LIST_STATIONS:
            return {
                ...state,
                items: action.data,
                country: action.country,
                stateprov: action.stateprov,
                city: action.city,
            };
        case GET_DATA_STATION:
            return {
                ...state,
                country: action.country,
                stateprov: action.stateprov,
                city: action.city,
                station: action.station
            };
        case GET_DATA_NEAREST_STATION:
            return {
                ...state,
            };
        default:
            return state;
    }
}


export default reducer