import {
    SIGN_IN_REQUEST,
    SIGN_IN_SUCCESS,
    SIGN_IN_FAILURE,
    SIGN_IN_RESET_STATUS,

    RESTORE_TOKEN,
    APP_LOADED,
    SIGN_OUT,

} from '../Iface/Types';


const initiSiState = {

    isLoading: true,
    isSignOut: true,
    userToken: null,
    username: '',

    signInResCode: 200,
    signInResMsg: '',
    signInReqIsLoading: false,
};

const reducer = (state = initiSiState, action) => {
    switch (action.type) {
        case SIGN_IN_REQUEST:
            return {
                ...state,
                signInReqIsLoading: true,
            };
        case SIGN_IN_SUCCESS:
            return {
                ...state,
                signInReqIsLoading: false,
                userToken: action.token,
                username: action.username,
                isSignOut: false,
            };
        case SIGN_IN_FAILURE:
            return {
                ...state,
                signInReqIsLoading: false,
                signInResCode: action.code,
                signInResMsg: action.msg,
            };
        case SIGN_IN_RESET_STATUS:
            return {
                ...state,
                signInResCode: 200,
                signInResMsg: '',
            };
        case RESTORE_TOKEN:
            return {
                ...state,
                userToken: action.token,
                username: action.username,
                isLoading: false,
                isSignOut: false,
            };
        case APP_LOADED:
            return {
                ...state,
                isLoading: false,
            };
        case SIGN_OUT:
            return {
                ...state,
                userToken: null,
                username: '',
                isSignOut: true

            };
        default:
            return state;
    }
}

export default reducer