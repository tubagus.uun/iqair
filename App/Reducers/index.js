import { combineReducers } from 'redux'
import AvReducer from './AvReducer'
import SignInReducer from './SignInReducer'

export default combineReducers({
    av: AvReducer,
    si: SignInReducer
})